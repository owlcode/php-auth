<?php

class Auth
{
    private $db = null;
    private $log = null;

    public function __construct($db)
    {
        session_start();
        session_regenerate_id();

        $this->checkUserAgent();

        $this->log = new Log();
        $this->db = $db;
        
        if(isset($_POST["login"]) && isset($_POST["pass"])) {
            try {
                if(!$this->login()) {
                    echo '<h1>Nie udalo się zalogować. Spróbuj ponownie.</h1>';
                }
            } catch (Exception $e) {
                $this->log->add($e);
            }
        }

        if(isset($_GET['logout'])) {
            if($this->logout()) {
                echo '<h1>Pomyślnie wylogowano</h1>';
            }
        }
    }
    
    private function login() {
        $pattern = "/^[a-z]+$/";
        $login = $this->db->real_escape_string($_POST['login']);

        if(preg_match($pattern, $login)) {
            $q = "select * from users where name = ?";
            if($sql = $this->db->prepare($q)) {
                $sql->bind_param('s', $login);
                $sql->execute();

                $res = $sql->get_result();

                while($row = $res->fetch_object()) {
                    if(password_verify($row->salt.$_POST['pass'], $row->pass)) {
                        $_SESSION['ssid'] = true;
                        $_SESSION['userid'] = $row->id;
                        $_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];

                        if($row->failed_login_ip != '0.0.0.0') {
                            echo '<h2 style="color:red;">Uwaga! Nieudana próba logowania: '.$row->failed_login.' z adresu IP '.$row->failed_login_ip.'</h2>';
                            $this->clearFailedLogin($row->id);
                        }

                        return true;
                    } else {
                        $this->failedAuth($row->id);
                        return false;
                    }
                }
            } else {
                $this->log->add(" Nie udało się przygotować zapytania".$login);
            }
        } else {
            $this->log->add(" Ktoś próbuje zalogować się złymi danymi...".$login);
        }
    }

    private function logout() {
        session_unset();
        session_destroy();
        return true;
    }

    private function failedAuth($id) {
        $id = (int)$id;
        $ip = $_SERVER['REMOTE_ADDR'];

        $sql = "update users set failed_login = NOW(), failed_login_ip =  '".$ip."' where id = $id";
        $this->db->query($sql);
        usleep(500000);
    }

    private function clearFailedLogin($id) {
        $sql = "update users set failed_login_ip =  '0.0.0.0' where id = $id";
        $this->db->query($sql);
    }

    public static function isLoggedIn() {
        if(isset($_SESSION['ssid']) && $_SESSION['ssid'] == true) {
            return true;
        }
        return false;
    }

    private function checkUserAgent() {
        if(isset($_SESSION['user_agent']) && $_SESSION['user_agent'] != $_SERVER['HTTP_USER_AGENT']) {
            $this->logout();
        }
    }

}
