<?php

class Note
{
    private $db = null;
    private $log = null;

    public function __construct($db)
    {
        $this->db = $db;
        $this->log = new Log();

        if (isset($_GET['note']) && $_GET['note'] == 'add') {
            $this->create();
        }
    }

    public function showAllNotes()
    {
        $res = $this->getUserNotes();
        $this->displayNotes($res);
    }

    public function showUserNotes($id)
    {
        $res = $this->getUserNotes($id);
        $this->displayNotes($res);
    }

    public function getUserNotes($id = "%")
    {
        $sql = $this->db->prepare("select *,users.name as username from notes, users where userid like ? and userid = users.id order by notes.id DESC");
        $sql->bind_param('s', $id);

        $sql->execute();

        return $sql->get_result();
    }

    private function displayNotes($res)
    {
        while ($row = $res->fetch_object()) {
            echo '<p><b>' . $row->username.' <small>'.$row->date . '</small> </b>';
            echo stripslashes(htmlspecialchars($row->text)) . '</p>';
        }
    }

    private function create()
    {
        if (Auth::isLoggedIn()) {
            $userid = (int)$_SESSION['userid'];

            $sql = $this->db->prepare("insert into notes values(null, NOW(), ?, ?)");
            $sql->bind_param('ss', $userid, $_POST['note']);

            if ($sql->execute()) {
                echo "<h1>Pomyślnie dodano notatkę. ID: " . $this->db->insert_id . "</h1>";
            } else {
                echo '<h1>Nie udało się dodać notatki :(</h1>';
                $this->log->add("Błąd execute sql Note.create()");
            }
        }
    }


}