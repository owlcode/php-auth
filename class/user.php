<?php

class User
{
    private $db = null;
    private $log = null;
    private $login = null;
    private $pass = null;
    
    public function __construct($db, $login, $pass)
    {
        $this->log = new Log();
        $this->db = $db;
        $this->login = $login;
        $this->pass = $pass;
    }

    /*
     * @returns
     * 0 - user created
     * 1 - user exist
     * 2 - validation error
     * 3 - db error
     */
    public function create()
    {
        $pattern = "/^[a-z]+$/";
        $ip = '0.0.0.0';
        $login = $this->db->real_escape_string($this->login);

        if (!preg_match($pattern, $this->login)) return 2;
        if ($this->userExist($login)) return 1;

        $size = mcrypt_get_iv_size(MCRYPT_CAST_256, MCRYPT_MODE_CFB);
        $salt = mcrypt_create_iv($size, MCRYPT_DEV_RANDOM);

        $salt = mb_convert_encoding($salt, "UTF-8");
        $this->pass = password_hash($salt . $this->pass, PASSWORD_BCRYPT);

        $q = "insert into users values(null, ?, ?, null, ?, null, ?)";
        if($sql = $this->db->prepare($q)) {
            $sql->bind_param('ssss', $this->login, $this->pass, $salt, $ip);

            if ($sql->execute()) {
                return 0;
            } else {
                return 3;
            }
        }
    }

    private function userExist($login)
    {
        $q = "select id from users where name = ?";
        if($sql = $this->db->prepare($q)) {
            $sql->bind_param('s', $login);
            $sql->execute();
            $sql->store_result();
        }

        return ($sql->num_rows);
    }
}