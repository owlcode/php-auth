<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="js/passchk_fast.js"></script>
    <script src="js/fun.js"></script>
    <title>Login PHP - Dawid Sowa</title>
</head>
<body>
<?php
    include_once 'config.php';
    include_once 'class/Log.php';
    include_once 'class/User.php';
    include_once 'class/Auth.php';
    include_once 'class/Note.php';

    $db = new mysqli(HOST, USER, PASS, DB);
    if (!$db->set_charset("utf8")) die();

    $log = new Log();
    $auth = new Auth($db);

    if(Auth::isLoggedIn()) {
        $note = new Note($db);
        
        require_once('post.html');

        $note->showAllNotes();
    } else {
        require_once('login.html');
    }

    if(isset($_GET['u']) && isset($_GET['p'])) {
        try {
            $user = new User($db, $_GET['u'], $_GET['p']);
            
            switch($user->create()) {
                case 0: echo '<h1>Pomyślnie zarejestrowano</h1>'; break;
                case 1: echo '<h1>Taki użytkownik istnieje</h1>';  break;
                case 2:
                    echo '<h1>Podałeś błędne dane</h1>';
                    throw new Exception(time()." Ktoś próbuje zarejestrować złe dane...".$_GET['u']);
                break;
                case 3:
                    echo '<h1>Podałeś błędne dane</h1>';
                    throw new Exception(time().' Błąd zapytania SQL (' . $this->db->connect_errno . '): ' . $this->db->connect_error . ')');
                break;
            }
        } catch (Exception $e) {
            $log->add($e->getMessage());
        }
    }

?>